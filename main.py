import sqlite3


def main():
    rows = range(25)
    db = sqlite3.connect('test.db')
    db.row_factory = sqlite3.Row
    db.execute('drop table if exists test')
    db.execute('create table test (t1 text, i1 int)')
    # Create data from a loop.
    i = 0
    while i < 100:
        db.execute('insert into test (t1, i1) values (?, ?)', ('section - '+str(i), i))
        i += 1
    db.commit()
    cursor = db.execute('select t1,i1 from test order by i1')

    for row in cursor:
        #print(dict(row))
        print(row['t1'])

if __name__ == '__main__':
    main()